## Архитектура приложения
Выбранная архитектура - монолит. 

### Преимущества: 

* Быстрый time-to-market, так как не нужно по отдельности настраивать и конфигурироовать каждый микросервис и дебажить его. 
* Нет нужды осваивать допольнительный инструментарий для managment-а микросервисов - k8s.
* Возможно значительное снижение материальных затрат, т.к. все приложение находиться на одном сервере.

### Недостатки:

* Платформа Node.JS плохо подходит для монолитных приложений, т.к. она - однопоточна (не беря во внимания worker_threads). Одним из решений может быть запуск нескольких node.js процессов на одной "машине" с настроенным load balancer-ом(nginx). **UPDATE: прочитав про то как Node.js запустить в многопоточном окружении, наткнулся на pm2 и модуль cluster**
* По мере роста приложения будет труднее изолировать команды и разрабатывать новый "фичи" по отдельности. Также по мере роста возникает проблема масштабируемости, например, если в "Twitter-clone-4252136" больше всего нагрузка приходиться на получения списка постов, а внутренним мессенджером никто не пользуеться, то гараздо проще добавить пару "инстансов" микросервисов на получения постов, чем добавлять целиком новый сервер.   
* Микросервисы щас в тренды :)

## Выбор протоколов
Выбранные протоколы: HTTP(s), WebSockets|SSE, REST API, RabbitMQ (AMQP, MQTT), WebRTC.

* Клиент-сервер будут общаться посредством первых 3 протоколов.
* RabbitMQ будет использываться для асинхронных задач: генерация PDF, QR-кода, email рассылка.
* WebRTC будет использываться для фото- и видеосъемки.
* WebSockets vs SSE? WS будут использываться для полно-дуплексного соедениния между сервером и клиентов, например чат. SSE будет использываться для "lightweight" соеденинием между сервером и клиентом, где инициатором будет сервер, например систему ачивок можно реализовать на SSE: была выполнена какая-та ачивка -> сервер отправляет посредством SSE ачивку клиенту. Зачем SSE когда есть WS? WS требуется для реал-таймового общения меджу клиентов и сервером и требует handshake и переключения протокола с HTTP -> WS, в то время когда SSE работает поверх HTTP и не требует каких-то допольнительных манипуляций. 

## Технологии
### СУБД
В качестве СУБД будет использываться NoSQL решение - Mongo.
#### Преимущества NoSQL(MongoDB)
* NoSQL БД намного легче масштабировать.
* NoSQL будет практически во всех случаях будет производительнее чем привычная РСУБД. РСУБД можно сделать производительнее: построить индексы, добиться index-only scan-a, оптимизировать запросы - кажеться простым, но это требует дополнительных знаний у инженера.
* NoSQL щас в тренде :)

#### Недостатки NoSQL (MongoDB)
* Отсуствия транзаций (до текущего момента), ACID.
* Непривычная логика мышления при проектировании структуры БД по сравнению с привычным реляционным подходом, когда все данные находяться в денормализованном виде (нарушения 3-ой нормальной формы).
* Еще много недостатков вытекает из предыдущего недостатка: любая модификая данных, должна модифицировать данные в зависимых коллекциях.

### CI/CD, Server
Сервер: IaaS от AWS - EC2. Хранилище медиа и исходников: S3 bucket. CI/CD: TravisCI, CodeDeploy и CodePipeline.

Детальный флоу: исходники находятся на github-e; после пул-реквеста на develop ветку, запускаеться travis-ci, который загружает весь репозиторий на S3 bucket; дальше codedeploy выкачивает исходники из бакета и деплоит на EC2 инстанс - этот pipeline брал из [тутуриала](https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html)).

База будет подниматься на том же инстансе, где и поднимаеться само приложениие.


### Структура проекта 
Структура проекта будет привидена ниже(для удобства) -  типичная для Node.JS веб-приложений: 

server

 - api
     - middlewares
     - services
     - routes
 - config
 
 - helpers/utils
 
 - data 
     - migraions
     - seeds
     - queries
	 
 - socket 

P.s. из названия директории, понятно за что отвечают файлы в директории.

### Напоследок

**Роли**

* Бизнес: тренер, бармен
* Админы
* Клиент

Роли будут распологаться в СУБД в виде enum, в одной из колонки в отношении `users`, у одного юзера может быть только одна роль.

**Разделение по доменах**

* wake-park.com
* business.wake-park.com
* admin.wake-park.com

Хотя разделение по доменах ведет к допольнительным расходам: покупка 3 разных доменных имен и 3 разных сервера, то для MVP можно и разделить на:

* wake-park.com - для клиентов
* wake-park.com/admin
* wake-park.com/business


#

## Application architecture
The chosen architecture is a monolith.

### Benefits:

* Fast time-to-market, as there is no need to separately configure and configure each microservice and debug it.
* There is no need to learn additional tools for managment microservices - k8s.
* A significant reduction in material costs is possible, because the whole application is on one server.

### Disadvantages:

* The Node.JS platform is poorly suited for monolithic applications because it is single-threaded (excluding worker_threads). One solution might be to run several node.js processes on one "machine" with a load balancer (nginx) configured.
* As the application grows, it will become more difficult to isolate teams and develop new "features" separately. Also, as it grows, the problem of scalability arises, for example, if in "Twitter-clone-4252136" most of the load falls on receiving a list of posts, and no one uses the internal messenger, then it is much easier to add a couple of "instances" of microservices to receive posts than to add a completely new server.
* Microservices are trending right now :)

## Selecting protocols
Selected protocols: HTTP (s), WebSockets | SSE, REST API, RabbitMQ (AMQP, MQTT), WebRTC.

* Client-server will communicate using the first 3 protocols.
* RabbitMQ will be used for asynchronous tasks: PDF generation, QR code generation, email distribution.
* WebRTC will be used for photo and video shooting.
* WebSockets vs SSE? WS will be used for full duplex communication between server and clients, such as chat. SSE will be used for "lightweight" connection between the server and the client, where the server will be the initiator, for example, the achievement system can be implemented on SSE: some kind of achievement has been performed -> the server sends the achievement to the client via SSE. Why SSE when there is WS? WS is required for real-time communication between clients and the server and requires handshake and protocol switching from HTTP -> WS, while SSE runs on top of HTTP and does not require any additional manipulations.

## Technology
### DBMS
NoSQL solution Mongo will be used as a DBMS.
#### Benefits of NoSQL (MongoDB)
* NoSQL databases are much easier to scale.
* NoSQL will in almost all cases be more efficient than the usual RDBMS. The RDBMS can be made more productive: build indexes, achieve index-only scan-a, optimize queries - it seems simple, but it requires additional knowledge from the engineer.
* NoSQL is trending right now :)

#### Disadvantages of NoSQL (MongoDB)
* Lack of transactions (until now), ACID.
* Unusual logic of thinking when designing a database structure compared to the usual relational approach, when all data is in denormalized form (violations of the 3rd normal form).
* Many more drawbacks follow from the previous drawback: any data modification must modify data in dependent collections.

### CI / CD, Server
Server: IaaS from AWS - EC2. Media and source repository: S3 bucket. CI / CD: TravisCI, CodeDeploy and CodePipeline.

Detailed flow: sources are on github-e; after a pull request to develop a branch, travis-ci is launched, which uploads the entire repository to the S3 bucket; then codedeploy downloads the source code from the bucket and deploy it to the EC2 instance - this pipeline was taken from the [tuturial] (https://docs.aws.amazon.com/codepipeline/latest/userguide/tutorials-simple-s3.html)).

The base will rise on the same instance where the application itself will rise.


### Project structure
The project structure will be shown below (for convenience) - typical for Node.JS web applications:

server

 - api
     - middlewares
     - services
     - routes
 - config
 
 - helpers / utils
 
 - data
     - migraions
     - seeds
     - queries

 - socket

P.s. from the name of the directory, it is clear what the files in the directory are responsible for.

### Finally

** Roles **

* Business: coach, bartender
* Admins
* Customer

Roles will be located in the RDBMS in the form of an enum, in one of the columns in relation to `users`, one user can have only one role.

** Separation by domain **

* wake-park.com
* business.wake-park.com
* admin.wake-park.com

Although division by domains leads to additional costs: buying 3 different domain names and 3 different servers, then for MVP it can be divided into:

* wake-park.com - for clients
* wake-park.com/admin
* wake-park.com/business